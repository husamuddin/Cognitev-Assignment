const Winston = require('winston')
const { format } = Winston
const { combine, timestamp, label, printf } = format

const logger = Winston.createLogger({
    format: combine(
        label({ label: 'nodeapp' }),
        timestamp(),
        printf(
            info => {
                const { timestamp, label, level, message, error } = info

                let log = `${timestamp} [${label}] ${level}: `

                if(level === 'error') {
                    log += '\n'
                    log += info.stack
                } else {
                    log += message
                }

                return log
            }
        )
    ),

    exitOnError: true,
    transports: [
        new Winston.transports.Console(),
    ],
})

module.exports = logger

