import { validationResult } from 'express-validator/check'
import errors from './errors'
const { ValidationError } = errors

export default (req, res, next) => {
    const errors = validationResult(req)

    if(!errors.isEmpty()) {
        errors.formatWith(({ location, msg, param, value, nestedErrors }) => {
            return { param, msg }
        })

        const customErrors = {}
        errors.array().forEach(
            (error) => {
                const {param, msg} = error
                if(!customErrors[param]) {
                    customErrors[param] = [{
                        error: msg
                    }]
                }
            }
        )

        return next(
            new ValidationError(customErrors)
        )
    }

    return next()
}

