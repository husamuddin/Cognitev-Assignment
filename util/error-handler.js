import logger from '../logger'

export default (error, req, res, next) => {
    if(error) {
        const { name, message } = error

        switch(name) {
            case 'ValidationError':
                logger.log('debug', error)
                return res.status(400).send(error.list || { error: error.message })

            case 'UnAuthorizedError':
                return res.status(401).send({ error: error.message })

            case 'UserNotFoundError':
                return res.status(400).send({
                    error: error.message
                })

            case 'InvalidCredentialsError':
                return res.status(400).send({
                    error: error.message
                })

            default:
                logger.log('error', error)
                return res.status(500).send(
                    { error: {name, description: message} }
                )
        }
    }

    return next()
}
