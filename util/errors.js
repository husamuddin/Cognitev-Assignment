class ValidationError extends Error {
    constructor(message = 'invalid') {
        if(typeof(message) === 'string') {
            super(message)
        } else {
            super('invalid')
            this.list = message
        }

        this.name = 'ValidationError'
    }
}

class InvalidCredentialsError extends Error {
    constructor(message = 'invalid credentials') {
        super(message)
        this.name = 'InvalidCredentialsError'
    }
}

class UserNotFoundError extends Error {
    constructor(message = 'user not found') {
        super(message)
        this.name = 'UserNotFoundError'
    }
}

class UnAuthorizedError extends Error {
    constructor(message = 'unauthorized action') {
        super(message)
        this.name = 'UnAuthorizedError'
    }
}

export default {
    ValidationError,
    InvalidCredentialsError,
    UserNotFoundError,
    UnAuthorizedError
}
