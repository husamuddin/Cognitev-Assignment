import mongoose from 'mongoose'
import { env } from 'process'
const { DATABASE } = env

export default mongoose.createConnection(DATABASE)

