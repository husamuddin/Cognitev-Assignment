import express from 'express'
import bodyParser from 'body-parser'

import { env } from 'process'
const { PORT, NODE_ENV } = env

import controllers from './controllers'
const { createUser, auth, updateStatus } = controllers

import errorHandler from './util/error-handler'
import logger from './logger'

/*
 * EXPRESS SETTING UP
 */
const app = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

/*
 * ROUTES
 */
app.all('/*', (req, res, next) => { // monitoring the requests
    logger.info(`${req.method} ${req.path}`)
    return next()
})
app.post('/user', createUser)
app.post('/auth', auth)
app.post('/update-status', updateStatus)

/*
 * HANDLING 404
 */
app.all('/*', (req, res, next) => {
    return res.status(404).send({
        error: 'not found'
    })
})

/*
 * HANDLING ERRORS
 */
app.use(errorHandler)

/*
 * SERVER LISTEN
 */
if(NODE_ENV !== 'test') {
    app.listen(PORT, error => {
        if(error) {
            return logger.error(error)
        }

        return logger.info(`nodeapp is listening internally on port ${PORT}`)
    })
}

export default app

