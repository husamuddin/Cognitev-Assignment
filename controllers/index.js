import multer from 'multer'
const upload = multer({ storage: multer.memoryStorage() })

import validation from '../validators'
const {
    userValidation,
    authValidation,
    checkValidation
} = validation
import validationErrorsHandler from '../util/validation-errors-handler'
import errors from '../util/errors'
const {
    ValidationError,
    InvalidCredentialsError,
    UserNotFoundError,
    UnAuthorizedError
} = errors

import User from '../models/user'
import bcrypt from 'bcrypt'

import jwt from 'jsonwebtoken'
const SECRET = '<the secret key>'

export default {
    createUser: [
        upload.single('avatar'),
        userValidation,
        validationErrorsHandler,
        (req, res, next) => {
            const { file: avatar } = req
            const {
                birthdate, country_code,
                email, first_name,
                gender, last_name,
                phone_number, password
            } = req.body

            const fileAsString = `data:${avatar.mimetype};base64,${avatar.buffer.toString('base64')}`

            const user = new User()
            user.avatar = fileAsString
            user.birthdate = birthdate
            user.country_code = country_code
            user.email = email
            user.first_name = first_name
            user.gender = gender
            user.last_name = last_name
            user.phone_number = phone_number

            const saltRounds = 10 // to be dynamic later
            user.password = bcrypt.hashSync(password, saltRounds)

            return user.save((error, doc) => {
                if(error) {
                    return next(error)
                }

                const {
                    birthdate,
                    country_code,
                    email,
                    first_name,
                    gender,
                    last_name,
                    phone_number
                } = doc

                return res.status(201).send({
                    birthdate,
                    country_code,
                    email,
                    first_name,
                    gender,
                    last_name,
                    phone_number
                })
            })

        }
    ],
    auth: [
        authValidation,
        validationErrorsHandler,
        (req, res, next) => {
            const {
                phone_number,
                password
            } = req.body

            return User.findOne({phone_number}, {password}, (error, doc) => {
                if(error) {
                    return next(error)
                }

                if(!doc) {
                    return next(
                        new UserNotFoundError
                    )
                }

                const { _id: id, password: hash } = doc
                if(!bcrypt.compareSync(password, hash)) {
                    return next(
                        new InvalidCredentialsError
                    )
                }

                const payload = { id }
                const authToken = jwt.sign(payload, SECRET)
                return res.send({ authToken })
            })
        }
    ],
    updateStatus: [
        checkValidation,
        validationErrorsHandler,
        (req, res, next) => {
            const { phone_number, auth_token: authToken , status } = req.body

            try {
                const payload = jwt.verify(authToken, SECRET)
                const user = User.findOne({_id: payload.id}, (error, doc) => {
                    if(error) {
                        return next(error)
                    }

                    if(!user) {
                        return next(
                            new UserNotFoundError
                        )
                    }

                    if(doc.phone_number !== phone_number) {
                        return next(new UnAuthorizedError())
                    }

                    doc.status = status
                    return doc.save(
                        (error, savedDoc) => {
                            if(error) {
                                return next(error)
                            }

                            const {
                                birthdate,
                                country_code,
                                email,
                                first_name,
                                gender,
                                last_name,
                                phone_number,
                                status,
                            } = savedDoc

                            return res.send({
                                birthdate,
                                country_code,
                                email,
                                first_name,
                                gender,
                                last_name,
                                phone_number,
                                status,
                            })
                        }
                    )
                })
            } catch(e) {
                if(e.name === 'JsonWebTokenError') {
                    return next(new ValidationError)
                } else {
                    return next(e)
                }
            }
        }
    ],
}
