const chai = require('chai')
const mongoose = require('mongoose')
const sinon = require('sinon')
const assert = chai.assert
const chaiAsPromised = require("chai-as-promised")
const app = require('../')

chai.use(chaiAsPromised)

describe('POST /user', () => {
    it(`should throw error if there is any field missed`)
    it(`should pass if all required fields exist`)
})

describe('POST /auth', () => {
    it(`should throw error if there is any field missed`)
    it(`should return the auth token when phone_number and password are given`)
})

describe('POST /update-status', () => {
    it(`should throw error if there is any field missed`)
    it(`should return user object with the new status`)
})

