import mongooseConnection from '../config/mongoose'
import mongoose from 'mongoose'

const Schema = mongoose.Schema

const User = mongooseConnection.model(
    'User',
    new Schema({
        avatar: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String,
            required: true
        },
        birthdate: {
            type: Date,
            required: true
        },
        country_code: {
            type: String,
            required: true
        },
        first_name: {
            type: String,
            require: true
        },
        gender: {
            type: String,
            required: true
        },
        last_name: {
            type: String,
            required: true
        },
        phone_number: {
            type: String,
            required: true,
            unique: true,
        },
        status: {
            type: String,
            required: false,
        },
    })
)

export default User
