# Cognitev Assignment
- the avatar field (which has a file type), I assumed that the requirement was to store the image in the database, which really not good idea. anyway. I made the application handle it as  [Data URL Schema](https://tools.ietf.org/html/rfc2397).
- for the failing responses, I made the the handler only show the first error of each field. I thought it would be more convenient this way. 
- the user schema, it was built as following
- for the auth token I just used jwt token holding the user id in its payload, it was odd that the request required the phone_number as a mandatory. but I used it anyway to double check that the user with in jwt payload has the same phone number of the given one.
- and for the status, I wasn't sure about it, but the most reasonable thought is that the status would be a field for the user, so. I assumed the request is for updating the status field of the user.
## Running the application
- the following command is for running docker-compose of the application in detached mode.
```sh
$ docker-compose up -d
```
- also you can monitor the customized logs (with winstonjs) with:
```sh
$ docker-composed logs -f nodeapp
```
- and for the tests run the following:
```sh
$ docker-composed exec nodeapp npm test # for the test 
$ docker-composed exec nodeapp npm run coverage # for the coverage
$ docker-composed exec nodeapp npm watch-test # for watch the test
```
- you can test the endpoints on postman from the following button:

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/4672ad92ca8f3f79d75a)
