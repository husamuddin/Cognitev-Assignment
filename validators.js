import { check, checkSchema } from 'express-validator/check'
import User from './models/user'

const isPhoneNumberExist = () => {
    return Promise.resolve(true)
}

const userValidation = [
    (req, res, next) => {
        const { file } = req

        const createError = (msg) => {
            if(!req._validationErrors) {
                req._validationErrors = []
            }

            req._validationErrors.push({
                location: 'files',
                param: 'avatar',
                value: file,
                msg
            })
        }

        try {
            const fileAnditsMimeTypeExist = file && file.mimetype

            if(req.body.avatar) {
                throw new Error('invalid')
            }

            if(!fileAnditsMimeTypeExist) {
                throw new Error('blank')
            }

            if(!file.mimetype.match(/^image\/.*/i)) {
                throw new Error('invalid_content_type')
            }
        } catch(e) {
            createError(e.message)
        }

        return next()
    },
    check('birthdate').exists().withMessage('blank'),
    check('birthdate').matches(/\d{4}-\d{2}-\d{2}/i).withMessage('invalid'),
    check('birthdate').custom((value) => {
        const givenDate = new Date(value)
        const now = new Date()

        if(givenDate > now) {
            throw new Error('invalid')
        }

        return true
    }).withMessage('in_the_future'),

    check('country_code').exists().withMessage('blank'),
    check('country_code').isISO31661Alpha2().withMessage('inclusion'),

    check('email').exists().withMessage('blank'),
    check('email').custom(
        email => {
            return new Promise(
                (resolve, reject) => {
                    return User.count({email}, (error, count) => {
                        if(error) {
                            throw error
                        }

                        if(count) {
                            return reject()
                        }

                        resolve()
                    })
                }
            )
        }
    ).withMessage('taken'), // check on the datebase
    check('email').isEmail().withMessage('invalid'),

    check('first_name').exists().withMessage('blank'),
    check('first_name').isAlpha().withMessage('invalid'),

    check('gender').exists().withMessage('blank'),
    check('gender').custom(
        (value) => {
            if(!!value.match(/male|female/i)) {
                return value
            }

            throw new Error('invalid')
        }
    ).withMessage('inclusion'),

    check('last_name').exists().withMessage('blank'),
    check('last_name').isAlpha().withMessage('invalid'),

    check('phone_number').exists().withMessage('blank'),
    check('phone_number').custom(
        (value) => {
            if(value.match(/^\+?[1-9]\d{1,14}$/)) {
                return value
            }

            throw new Error('invalid')
        }
    ).withMessage('invalid'),
    check('phone_number').isLength({ min: 10 }).withMessage('too_short'),
    check('phone_number').isLength({ max: 15 }).withMessage('too_long'),
    check('phone_number').custom(
        async (value) => {
            const isExist = await isPhoneNumberExist(value)
            if(isExist) {
                return value
            }

            throw new Error('invalid')
        }
    ).withMessage('not_exist'),
    check('phone_number').custom(
        phoneNumber => {
            return new Promise(
                (resolve, reject) => {
                    return User.count({
                        phone_number: phoneNumber
                    }, (error, count) => {
                        if(error) {
                            throw error
                        }

                        if(count) {
                            return reject()
                        }

                        return resolve()
                    })
                }
            )
        }
    ).withMessage('taken'),

    check('password').exists().withMessage('blank'),
    check('password').isLength({ min: 6 }).withMessage('too_short'),
]

const authValidation = [
    check('phone_number').exists().withMessage('blank'),
    check('phone_number').custom(
        (value) => {
            if(value.match(/^\+?[1-9]\d{1,14}$/)) {
                return value
            }

            throw new Error('invalid')
        }
    ).withMessage('invalid'),
    check('phone_number').isLength({ min: 10 }).withMessage('too_short'),
    check('phone_number').isLength({ max: 15 }).withMessage('too_long'),
    check('phone_number').custom(
        async (value) => {
            const isExist = await isPhoneNumberExist(value)
            if(isExist) {
                return value
            }

            throw new Error('invalid')
        }
    ).withMessage('not_exist'),

    check('password').exists().withMessage('blank'),
    check('password').custom(password => !!password.length).withMessage(
        'invalid'
    ),
    check('password').isLength({ min: 6 }).withMessage('too_short'),
]

const checkValidation = [
    check('phone_number').exists().withMessage('blank'),
    check('phone_number').custom(
        (value) => {
            if(value.match(/^\+?[1-9]\d{1,14}$/)) {
                return value
            }

            throw new Error('invalid')
        }
    ).withMessage('invalid'),
    check('phone_number').isLength({ min: 10 }).withMessage('too_short'),
    check('phone_number').isLength({ max: 15 }).withMessage('too_long'),
    check('phone_number').custom(
        async (value) => {
            const isExist = await isPhoneNumberExist(value)
            if(isExist) {
                return value
            }

            throw new Error('invalid')
        }
    ).withMessage('not_exist'),

    check('status').exists().withMessage('blank'),
    check('status').custom(
        password => !!password.length
    ).withMessage(
        'invalid'
    ),

    check('auth_token').exists().withMessage('blank'),
    check('auth_token').custom(
        authToken => !!authToken.length
    ).withMessage(
        'invalid'
    ),
]

const validation = {
    userValidation,
    authValidation,
    checkValidation,
}

export default validation
